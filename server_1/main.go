package main

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"log"
	"strings"
	xj "github.com/basgys/goxml2json"
)

func getXML(url string) (string, error) {
    resp, err := http.Get(url)
    if err != nil {
        return "", fmt.Errorf("GET error: %v", err)
    }
    defer resp.Body.Close()

    if resp.StatusCode != http.StatusOK {
        return "", fmt.Errorf("Status error: %v", resp.StatusCode)
    }

    data, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        return "", fmt.Errorf("Read body: %v", err)
    }

    return string(data), nil
}
func getContent(url string) ([]byte, error) {
    resp, err := http.Get(url)
    if err != nil {
        return nil, fmt.Errorf("GET error: %v", err)
    }
    defer resp.Body.Close()

    if resp.StatusCode != http.StatusOK {
        return nil, fmt.Errorf("Status error: %v", resp.StatusCode)
    }

    data, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        return nil, fmt.Errorf("Read body: %v", err)
    }

    return data, nil
}

func dataBMKG(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if data, err := getContent("https://data.bmkg.go.id/autogempa.xml"); err != nil {
		log.Printf("Failed to get XML: %v", err)
	} else {
		xml :=string(data)
		xml2 := strings.NewReader(xml)
		result, err := xj.Convert(xml2)
			if err != nil {
				panic("That's embarrassing...")
			}
			ra :=result.String();
			fmt.Println("data berhasil diambil")
			w.Write([]byte(ra));
	}
}

func main() {
	http.HandleFunc("/", dataBMKG)
    http.ListenAndServe(":4321", nil)
    fmt.Println("localhost:4321")
}