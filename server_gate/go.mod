module server_gate

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/go-redis/redis/v7 v7.4.0
	github.com/myesui/uuid v1.0.0 // indirect
	github.com/twinj/uuid v1.0.0
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.1 // indirect
	gopkg.in/stretchr/testify.v1 v1.2.2 // indirect
)
